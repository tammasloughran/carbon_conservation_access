#!/usr/bin/env python3
"""Check conservation of carbon in an ACCESS-ESM-1.5 simulation for wood products pools only.
This is done as Delta(Cpools) = flux.
Cpools = sum(wood_harvest_n)
flux = sum(wood_respiration_n) - wood_flux

Author: Tammas Loughran
Contact: tammas.loughran @ csiro.au
"""
import glob
import sys

import cartopy.crs as ccrs
import matplotlib as mpl
import matplotlib.pyplot as plt
import netCDF4 as nc
import numpy as np

DEFAULT_INPUT_DIR = '/g/data/p73/archive/CMIP6/ACCESS-ESM1-5/PI-02/history/atm/netCDF/'
CELL_AREA_FILE = '/g/data/fs38/publications/CMIP6/CMIP/CSIRO/ACCESS-ESM1-5/piControl/r1i1p1f1/fx/'\
        'areacella/gn/latest/areacella_fx_ACCESS-ESM1-5_piControl_r1i1p1f1_gn.nc'
CELL_AREA_VNAME = 'areacella'
TILE_FRAC_VNAME = 'fld_s03i317' # Surface tile fraction.
LAND_FRAC_VNAME = 'fld_s03i395' # Land area fraction.
NPP_VNAME = 'fld_s03i291' # Net primary productivity (on PFTs - 13).
RH_VNAME = 'fld_s03i173' # Heterotrophic (soil) respiration (on TILES - 17).
LAB_VNAME = 'fld_s03i851' # Labile carbon pool.
LEAF_VNAME = 'fld_s03i852' # Plant leaf carbon pool.
WOOD_VNAME = 'fld_s03i853' # Plant wood carbon pool.
ROOT_VNAME = 'fld_s03i854' # Plant root carbon pool.
METB_VNAME = 'fld_s03i855' # Litter metabolic carbon pool.
STR_VNAME = 'fld_s03i856' # Litter structural carbon pool.
CWD_VNAME = 'fld_s03i857' # Litter coarse woody debris carbon pool.
MIC_VNAME = 'fld_s03i858' # Soil microbial carbon pool.
SLOW_VNAME = 'fld_s03i859' # Soil slow carbon pool.
PASS_VNAME = 'fld_s03i860' # Soil passive carbon pool.
WHC1_VNAME = 'fld_s03i898' # Wood harvest carbon pool 1 (1 year).
WHC2_VNAME = 'fld_s03i899' # Wood harvest carbon pool 2 (10 year).
WHC3_VNAME = 'fld_s03i900' # Wood harvest carbon pool 3 (100 year).
RWC1_VNAME = 'fld_s03i907' # Wood harvest respiration (decay) of carbon (1 year).
RWC2_VNAME = 'fld_s03i908' # Wood harvest respiration (decay) of carbon (10 year).
RWC3_VNAME = 'fld_s03i909' # Wood harvest respiration (decay) of carbon (100 year).
WOODFLUX_VNAME = 'fld_s03i895' # Carbon flux to wood products.
CARBON_POOL_VARS = [
        LAB_VNAME,
        LEAF_VNAME,
        WOOD_VNAME,
        ROOT_VNAME,
        METB_VNAME,
        STR_VNAME,
        CWD_VNAME,
        MIC_VNAME,
        SLOW_VNAME,
        PASS_VNAME,
        ]
WOOD_HARVEST_POOL_VARS = [WHC1_VNAME,WHC2_VNAME,RWC3_VNAME]
WOOD_RESPIRATION_VARS = [RWC1_VNAME,RWC2_VNAME,RWC3_VNAME]
GINPG = 1e15 # Number of g in Pg.
KGINPG = 1e12 # Number of kg in Pg.
NYEARS = 5 # Number of years over which to check carbon conservation.
NTILES = 17 # Number of tiles.
NPFTS = 13 # Number of tiles that are plant functional types.
PFTS = [
        'EGNL',
        'EGBL',
        'DCNL',
        'DCBL',
        'shrub',
        'C3 grass',
        'C4 grass',
        'tundra',
        'C3 crop',
        'C4 crop',
        'wetland',
        'PFT12',
        'PFT13',
        ]
COLORS = [
        'blue',
        'green',
        'yellow',
        'salmon',
        'slategray',
        'pink',
        'skyblue',
        'gray',
        'seagreen',
        'coral',
        'steelblue',
        ]


def clean_data(data:np.ndarray)->np.ndarray:
    """Clean some data to have reasonable values.
    """
    data[data>1e19] = 0
    data[data<-1] = 0
    return data


def plot_globe_diverging(
            lats:np.ndarray,
            lons:np.ndarray,
            data:np.ndarray,
            title:str,
            filename:str,
            cmap:str='bwr',
            )->None:
    """Plot a global map of some data using the Robinson projection and a
    discrete colormap.
    """
    plt.figure()
    ax = plt.axes(projection=ccrs.PlateCarree())
    absmax = max(abs(np.min(data)), np.max(data))
    plt.pcolormesh(lons, lats, data,
            vmin=-absmax,
            vmax=absmax,
            cmap=cmap,
            transform=ccrs.PlateCarree(),
            )
    ax.coastlines()
    cbar = plt.colorbar(
            orientation='horizontal',
            pad=0.05,
            )
    cbar.set_label('Pg(C) year-1')
    plt.title(title)
    plt.tight_layout()
    plt.savefig(filename, dpi=200)


def yearly_sum_from_monthly(data:np.ndarray)->np.ndarray:
    """Calculate a days-per-month weighted yearly sum of monthly data.
    """
    if np.mod(data.shape[0], 12)!=0:
        raise ValueError("Not enough months in 0th dimension.")
    toshape = list(data.shape)
    toshape.pop(0)
    toshape.insert(0, 12)
    toshape.insert(0, int(data.shape[0]/12))
    days_per_month = np.array([31,28,31,30,31,30,31,31,30,31,30,31])
    data_per_month = data*days_per_month[None,:,None,None,None]
    return np.sum(data_per_month, axis=1)


def main(input_dir):
    # Collect a list of monthly (pa) netCDF files that need to be loaded.
    pa_files = sorted(glob.glob(input_dir+'/*pa*.nc'))
    dec_files = [fname for fname in pa_files if '12' in fname[-9:]]

    # Load the area.
    areanc = nc.Dataset(CELL_AREA_FILE, 'r')
    cell_area = areanc.variables[CELL_AREA_VNAME][:]
    areanc.close()
    ncfile = nc.Dataset(pa_files[0], 'r')
    land_frac = ncfile.variables[LAND_FRAC_VNAME][:].squeeze()

    # Load some grid description data.
    latname = ncfile.variables[LAND_FRAC_VNAME].dimensions[1]
    lats = ncfile.variables[latname][:]
    lons = ncfile.variables['lon'][:]
    ncfile.close()
    nlats = len(lats)
    nlons = len(lons)

    # Create arrays.
    cland = np.zeros((NYEARS,NTILES,nlats,nlons))
    wh1 = np.zeros((NYEARS,NTILES,nlats,nlons))
    wh2 = np.zeros((NYEARS,NTILES,nlats,nlons))
    wh3 = np.zeros((NYEARS,NTILES,nlats,nlons))
    rw = np.zeros((NYEARS,NTILES,nlats,nlons))
    wood_flux = np.zeros((NYEARS,NTILES,nlats,nlons))

    # Loop over december files.
    # Pool values are end-of-month values; so December files means the start of the next year.
    for year,fname in enumerate(dec_files[:NYEARS]):
        print("Loading: ", fname)
        ncfile = nc.Dataset(fname, 'r')

        # Load the tile fractions.
        tile_frac = ncfile.variables[TILE_FRAC_VNAME][:].squeeze()

        # Load the wood harvest pools. Units in the file are g m-2. They are converted to Pg.
        # These do not need to be multiplied by the tile fractions; they are already included.
        # Simulations without land-use change do not have wood products, so it may be skipped.
        wh1[year] = ncfile.variables[WHC1_VNAME][:].squeeze()
        wh2[year] = ncfile.variables[WHC2_VNAME][:].squeeze()
        wh3[year] = ncfile.variables[WHC3_VNAME][:].squeeze()

        # Load the wood respiration. Units in file are g m-2 year-1.
        # We only need one month because the units are already year-1 and there is no
        # variation throughout the year. Similar to wood_harvest, the fractions are already
        # included. Simulations without land-use change do not have wood respiration, so it may
        # be skipped.
        rw_tmp = 0.0
        for var in WOOD_RESPIRATION_VARS:
            if var in ncfile.variables.keys():
                rw_tmp += ncfile.variables[var][:].squeeze()

        rw[year,...] = rw_tmp

        # Load the wood_flux to product pools due to land-use and land-use change.
        # Units in file are g m-2 year-1. There is no seasonal variation so only 1 value is needed
        # per year.
        if WOODFLUX_VNAME in ncfile.variables.keys():
            wood_flux[year,...] = np.array(
                    ncfile.variables[WOODFLUX_VNAME][:]
                    ).squeeze() # g m-2 year-1

        ncfile.close()

    wh1 = clean_data(wh1)
    wh2 = clean_data(wh2)
    wh3 = clean_data(wh3)
    rw = clean_data(rw)
    cland = wh1 + wh2 + wh3

    # Clean missing values
    rw = clean_data(rw)
    wood_flux = clean_data(wood_flux)

    # Year-to-year variations in the carbon pools.
    delta_cland = np.diff(cland, axis=0) # g m-2 year-1

    # Calculate flux
    flux = wood_flux - rw

    # The 0th element of of delta_cland corresponds to the 1st element of flux.
    test = flux[1:] - delta_cland

    # Plot maps of time snapshots
    xx,yy = np.meshgrid(lons-1, lats)
    total_test = np.sum(test*cell_area, axis=1)/GINPG
    plot_globe_diverging(
            yy,
            xx,
            total_test[0],
            'flux - $\Delta$cLand year 0',
            'imbalance_year_0.png',
            )
    plot_globe_diverging(
            yy,
            xx,
            total_test[2],
            'flux - $\Delta$cLand year 2',
            'imbalance_year_2.png',
            )

    # Plot a time series of the total global difference per PFT.
    imbalance = np.sum(test*cell_area, axis=(-1,-2))/GINPG
    plt.figure()
    for i in range(NPFTS-2):
        plt.plot(imbalance[:,i], label=PFTS[i])
    plt.ylabel('Pg(C) year-1')
    plt.xlabel('Time (year)')
    plt.legend(loc='lower right')
    plt.title('Total global carbon imbalance per PFT')
    plt.savefig('total_global_carbon_imbalance_per_pft.png')

    # Total imbalance over all PFTs.
    imbalance_all_pfts = np.sum(imbalance, axis=1)
    plt.figure()
    plt.plot(imbalance_all_pfts)
    plt.ylabel('Pg(C) year-1')
    plt.xlabel('Time (year)')
    plt.title('Total global carbon imbalance all PFTs')
    plt.savefig('total_global_carbon_imbalance_all_pfts.png')

    # Plot the cumulative total global imbalance over all PFTs.
    plt.figure()
    plt.plot(np.cumsum(imbalance_all_pfts))
    plt.ylabel('Pg(C) year-1')
    plt.xlabel('Time (year)')
    plt.title('Total cumulative global carbon imbalance all PFTs')
    plt.savefig('total_cumulative_global_carbon_imbalance_all_pfts.png')

    # Plot the cumulative total for each PFT.
    plt.figure()
    for i in range(NPFTS-2):
        plt.plot(np.cumsum(imbalance[:,i], axis=0), label=PFTS[i])
    plt.ylabel('Pg(C) year-1')
    plt.xlabel('Time (year)')
    plt.title('Total cumulative global carbon imbalance per PFTs')
    plt.legend(loc='lower right')
    plt.savefig('total_cumulative_global_carbon_imbalance_per_pfts.png')

    # Plot a grid point. This is somewhere in Europe. I chose this one because there is land use
    # change and wood thinning in the 1850s.
    # Eastern Europe
    #i = 15
    #j = 114
    # Germany-ish
    i = 5
    j = 108
    # USA
    #i = 145
    #j = 101
    plt.figure()
    plt.plot(np.sum(flux[1:,:,j,i], axis=1), label='flux')
    plt.plot(np.sum(rw[1:,:,j,i], axis=1), label='rw')
    plt.plot(np.sum(wood_flux[1:,:,j,i], axis=1), label='wood_flux')
    plt.plot(np.sum(delta_cland[:,:,j,i], axis=1), label='$\Delta$cLand')
    plt.ylabel('Pg(C) year-1')
    plt.xlabel('Time (year)')
    plt.title(f'Absolute gridpoint ({i},{j}) flux & $\Delta$cLand')
    plt.legend(loc='lower right')
    plt.savefig(f'absolute_gridpoint_({i},{j}).png')

    plt.figure()
    for n in range(NPFTS-2):
        plt.plot(flux[1:,n,j,i]*cell_area[j,i]/GINPG, label=f'flux {PFTS[n]}', color=COLORS[n])
        plt.plot(delta_cland[:,n,j,i]*cell_area[j,i]/GINPG, label=f'$\Delta$cLand {PFTS[n]}', color='light'+COLORS[n])
    plt.ylabel('Pg(C) year-1')
    plt.xlabel('Time (year)')
    plt.title(f'Absolute gridpoint ({i},{j}) flux & $\Delta$cLand per PFT')
    plt.legend(loc='lower right')
    plt.savefig(f'absolute_gridpoint_({i},{j})_per_pft.png')

    # Plot the same thing except expressed as a % of the flux
    plt.figure()
    flux_sum = np.sum(flux[1:,:,j,i], axis=1)
    delta_cland_sum = np.sum(delta_cland[:,:,j,i], axis=1)
    percentage = 100*(flux_sum - delta_cland_sum)/flux_sum
    plt.plot(percentage, label='flux')
    plt.ylabel('%')
    plt.xlabel('Time (year)')
    plt.title(f'Carbon imballance relative to magnitude of flux.')
    plt.savefig(f'imbalance_realative_to_flux_({i},{j}).png')

    plt.show()


if __name__=='__main__':
    if len(sys.argv)==1:
        main(DEFAULT_INPUT_DIR)
    else:
        main(sys.argv[1])

