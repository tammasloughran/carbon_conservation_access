# Carbon conservation checks

These scripts use ACCESS-ESM1.5 output in netCDF format to chack carbon is conserved.
The conservation check is that the sum of the fluxes is equal to the year-to-year changes in the
carbon pools.

`check_carbon_conservation_grid_cell.py` checks the entire carbon cycle including the natural and
wood product pools.
`check_carbon_conservation_natural_only.py` checks the natural pools only.
`check_carbon_conservation_wood_products_only.py` checks the wood products pools only.

## How to use

The arguments of these scripts should be the directory where the netCDF files are located.

```bash
python3 check_carbon_conservation_grid_cell.py <dir>
```
